<?php
// Get categories for this specific post
$category_names = [];
foreach(get_the_category($post) as $category) {
	$category_names[] = $category->name;
}
?>

<div class="idea_block">
	<div class="idea_block_content_container">
		<div class="idea_block_content">
			<?php if(get_field('idea_post_external_link')): ?>
				<h2>
					<a href="<?php the_field('idea_post_external_link'); ?>" target="_blank"><?php the_title(); ?></a>
				</h2>
			<?php else: ?>
				<h2>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h2>
			<?php endif; ?>
			<span class="idea_post_meta"><?php the_date('m/d/Y'); ?> | <?php echo implode(', ', $category_names); ?></span>
			<?php if(get_field('idea_post_excerpt')): ?>
				<?php the_field('idea_post_excerpt'); ?>
			<?php endif; ?>
		</div>
	</div>
	<?php if(get_field('idea_post_media_type') === 'video'): ?>
		<div class="idea_block_media">
			<?php the_field('idea_post_video_embed'); ?>
		</div>
	<?php else: ?>
		<div class="idea_block_media" style="background-image:url(<?php the_field('idea_post_image'); ?>);"></div>
	<?php endif; ?>
</div>