<?php
/**
 * _s functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package _s
 */

if ( ! function_exists( '_s_setup' ) ) :

/*=========================
Sets up theme defaults and registers support for various WordPress features.
 
Note that this function is hooked into the after_setup_theme hook, which
runs before the init hook. The init hook is too late for some features, such
as indicating support for post thumbnails.
========================*/

function _s_setup() {

	// Add default posts and comments RSS feed links to head.

	add_theme_support( 'automatic-feed-links' );

	/*==========================================
	LET WORDPRESS MANAGE THE DOCUMENT TITLE
	==========================================*/

	add_theme_support( 'title-tag' );

	/*==========================================
	ENABLE SUPPORT FOR POST THUMBNAILS ON POSTS AND PAGES
	==========================================*/

	add_theme_support( 'post-thumbnails' );

	/*==========================================
	SETUP NAVIGATION MENUS
	==========================================*/

	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', '_s' ),
	) );

	/*==========================================
	Switch default core markup for search form, comment form, and comments
	to output valid HTML5.
	==========================================*/

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

}
endif;

add_action( 'after_setup_theme', '_s_setup' );

 // Set the content width in pixels, based on the theme's design and stylesheet.

function _s_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
}
add_action( 'after_setup_theme', '_s_content_width', 0 );

/*==========================================
REMOVE WP EMOJI
==========================================*/

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/*==========================================
REGISTER WIDGET AREA
==========================================*/

function _s_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', '_s' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', '_s_widgets_init' );

/*==========================================
ENQUEUE SCRIPTS AND STYLES
==========================================*/

function _s_scripts() {
	
	// Default theme style

	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	// Wordpress Default Jquery
	
	if (!is_admin()) {
		wp_enqueue_script('jquery');
	}

	// Compiled SCSS File

	wp_enqueue_style( 'custom_styles', get_template_directory_uri() . '/css/style.min.css' );

	// Flexslider Styles

	wp_enqueue_style('flexslider', get_template_directory_uri() . '/css/plugins/flexslider/flexslider.min.css');

	wp_enqueue_script( '_s-flexslider', get_template_directory_uri() . '/js/plugins/flexslider/jquery.flexslider-min.js', '','', true);

	// FitVids

	wp_enqueue_script('_s-fitvids', get_template_directory_uri() . '/js/plugins/fitvids/fitvids.min.js', '', '', true);

	// Font Awesome

	wp_enqueue_script('font-awesome', 'https://use.fontawesome.com/bfa5b116b0.js', '', '', true);

	// Source Sans Pro Google Font

	wp_enqueue_style('source-sans-pro', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600');

	// Velocity JS

	wp_enqueue_script('velocity-js', get_template_directory_uri() . '/js/plugins/velocity.min.js', '', '', true);

	// Ajax Script

	wp_enqueue_script('ajax', get_template_directory_uri() . '/js/pages_ajax.js', '', '', true);

	// Custom Scripts

	wp_enqueue_script('_s-scripts', get_template_directory_uri() . '/js/scripts.min.js', '', '', true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Ajax Load More

	wp_enqueue_script('ajax-load-more', ALM_URL . '/core/js/ajax-load-more.min.js'); 

}

add_action( 'wp_enqueue_scripts', '_s_scripts' );

/*====================================================================

Enqueue Typekit if Neccesary

function theme_typekit() {
    wp_enqueue_script( 'theme_typekit', '//use.typekit.net/mjs0clp.js');
}
add_action( 'wp_enqueue_scripts', 'theme_typekit' );

function theme_typekit_inline() {
  if ( wp_script_is( 'theme_typekit', 'done' ) ) { ?>
  	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<?php }
}
add_action( 'wp_head', 'theme_typekit_inline' );

====================================================================*/

/*==========================================
CUSTOM POST TYPES
==========================================*/

/*=== Portfolio ===*/

function custom_post_type_portfolio() {

	$labels = array(
		'name'                => ('Portfolio'),
		'singular_name'       => ('Portfolio'),
		'menu_name'           => ('Portfolio'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Projects'),
		'view_item'           => ('View Project'),
		'add_new_item'        => ('Add New Project'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Project'),
		'update_item'         => ('Update Project'),
		'search_items'        => ('Search Projects'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('portfolio'),
		'description'         => ('Portfolio'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'page-attributes' ),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'portfolio'),
		'taxonomies'          => array( 'portfolio_category' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-admin-site',
	);

	register_post_type( 'az_portfolio', $args );

}

add_action( 'init', 'custom_post_type_portfolio', 0 );

/*=== Team ===*/

function custom_post_type_team() {

	$labels = array(
		'name'                => ('Team'),
		'singular_name'       => ('Team'),
		'menu_name'           => ('Team'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Members'),
		'view_item'           => ('View Member'),
		'add_new_item'        => ('Add New Member'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Member'),
		'update_item'         => ('Update Member'),
		'search_items'        => ('Search Members'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('team'),
		'description'         => ('Team'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'page-attributes' ),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'team'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-groups',
	);

	register_post_type( 'az_team', $args );

}

add_action( 'init', 'custom_post_type_team', 0 );

/*==========================================
CUSTOM TAXONOMY 
==========================================*/

/*=== Stories Taxonomy ===*/

function add_portfolio_taxonomy() {
	$labels = array(
		'name' => ('Category'),
      	'singular_name' => ('Category'),
      	'search_items' =>  ('Search Categories' ),
      	'all_items' => ('All Categories' ),
      	'parent_item' => ('Parent Category' ),
      	'parent_item_colon' => ('Parent Category:' ),
      	'edit_item' => ('Edit Category' ),
      	'update_item' => ('Update Category' ),
      	'add_new_item' => ('Add New Category' ),
      	'new_item_name' => ('New Category Name' ),
      	'menu_name' => ('Categories' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'genre' ),
	);

	register_taxonomy( 'portfolio_category', array( 'post' ), $args );
}

add_action( 'init', 'add_portfolio_taxonomy', 0 );

/*==========================================
LIMIT POST REVISIONS
==========================================*/

function limit_post_revisions( $num, $post ) {
    $num = 3;
    return $num;
}

add_filter( 'wp_revisions_to_keep', 'limit_post_revisions', 10, 2 );

/*=============================================
CUSTOM LOGIN SCREEN
=============================================*/

// Change the login logo URL

function my_loginURL() {
    return esc_url( home_url( '/' ) );
}

add_filter('login_headerurl', 'my_loginURL');

// Enqueue the login specific stylesheet for design customizations.

function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/css/login.min.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');

/*=============================================
YOAST
=============================================*/

// Adjust Metabox Priority

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

/*=============================================
DISALLOW FILE EDIT
Remove the ability to edit theme and plugins via the wp-admin.
=============================================*/

function disable_file_editting() {
  define('DISALLOW_FILE_EDIT', TRUE);
}

add_action('init','disable_file_editting');

/*==========================================
INCLUDES
==========================================*/

/*== Custom template tags for this theme. ==*/

require get_template_directory() . '/includes/template-tags.php';

/*== Custom functions that act independently of the theme templates. ==*/

require get_template_directory() . '/includes/extras.php';
