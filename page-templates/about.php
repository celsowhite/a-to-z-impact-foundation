<?php
/*
Template Name: About
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

			<?php get_template_part('template-parts/header_image'); ?>

			<div class="page_content_container header_image_padding">
				<div class="loading_bar"></div>
				<div class="page_content">
					<div class="container">

						<!-- How We Work -->

						<section class="az_section">

							<h1 class="header_grey">How We Work</h1>
							<div class="az_row how_we_work_grid">
								<?php while(have_rows('how_we_work_attribute')): the_row(); ?>
									<div class="column_1_2">
										<h2><?php the_sub_field('how_we_work_title'); ?></h2>
										<p><?php the_sub_field('how_we_work_description'); ?></p>
									</div>
								<?php endwhile; ?>
							</div>

						</section>

						<!-- Our Team -->

						<section class="az_section">

							<h1 class="header_grey">Our Team</h1>
							<div class="az_row_small_gutter">
								<?php
									$team_args = array('post_type' => 'az_team', 'posts_per_page' => -1);
									$team_loop = new WP_Query($team_args);
									if ( $team_loop->have_posts() ) : while ( $team_loop->have_posts() ) : $team_loop->the_post();
								?>
									<div class="column_1_4 team_member_block">
										<div class="team_member_image_container">
											<?php the_post_thumbnail(); ?>
										</div>
										<h2><?php the_title(); ?></h2>
										<p class="team_title"><b><?php the_field('team_member_title'); ?></b></p>
										<div class="hidden_team_description"><?php the_content(); ?></div>
									</div>
								<?php endwhile; wp_reset_postdata(); endif; ?>
							</div>

						</section>

						<?php if(have_rows('partners')): ?>

							<!-- Our Partners -->

							<section class="az_section">

								<h1 class="header_grey">Our Partners</h1>
								<div class="az_row_small_gutter">
									<?php while(have_rows('partners')): the_row(); ?>
										<div class="column_1_4">
											<a href="<?php the_sub_field('partner_link'); ?>" target="_blank" class="partner_logo_container">
												<img src="<?php the_sub_field('partner_logo'); ?>" />
											</a>
										</div>
									<?php endwhile; ?>
								</div>

							</section>

						<?php endif; ?>

					</div>
				</div>
			</div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>
