<?php
/*
Template Name: How We Fund
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

			<?php get_template_part('template-parts/header_image'); ?>

			<div class="page_content_container header_image_padding">
				<div class="loading_bar"></div>
				<div class="page_content">
					<div class="single_column_container">
						<?php the_content(); ?>
					</div>
					<section class="what_we_are_looking_for">
						<div class="container">
							<h1 class="header_grey">What We Are Looking For</h1>
						</div>
						<div class="expandable_panels">
							<?php while(have_rows('how_we_fund_panels')): the_row(); ?>
								<div class="individual_panel" style="background-image: url(<?php the_sub_field('individual_panel_image'); ?>);">
									<div class="individual_panel_content">
										<h2><?php the_sub_field('individual_panel_title'); ?></h2>
										<p><?php the_sub_field('individual_panel_text'); ?></p>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
					</section>
				</div>
			</div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>
