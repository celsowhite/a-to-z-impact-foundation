<?php
/*
Template Name: Partner With Us
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

			<?php get_template_part('template-parts/header_image'); ?>

			<div class="page_content_container header_image_padding">
				<div class="loading_bar"></div>
				<div class="page_content">
					<div class="container">
						<div class="az_row">
							<div class="column_1_2">
								<?php the_content(); ?>
							</div>
							<div class="column_1_2">
								<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, '', $echo = true ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>
