<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

			<div class="homepage_slider flexslider">
				<div class="homepage_slider_content_container">
					<div class="container">
						<div class="homepage_slider_content">
							<h1 class="large"><?php the_field('homepage_slideshow_title'); ?></h1>
							<p><?php the_field('homepage_slideshow_description'); ?></p>
						</div>
					</div>
				</div>
				<ul class="slides">
					<?php $images = get_field('homepage_slideshow_images'); ?>
					<?php foreach($images as $image): ?>
						<li style="background-image: url('<?php echo $image['sizes']['large']; ?>');"></li>
					<?php endforeach; ?>
				</ul>
			</div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>
