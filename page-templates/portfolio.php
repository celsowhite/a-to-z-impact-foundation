<?php
/*
Template Name: Portfolio
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

			<?php get_template_part('template-parts/header_image'); ?>

			<div class="page_content_container header_image_padding">
				<div class="loading_bar"></div>
				<div class="page_content">
					<div class="container">
						<div class="az_row portfolio_grid">
							<?php
								$portfolio_args = array('post_type' => 'az_portfolio', 'posts_per_page' => -1);
								$portfolio_loop = new WP_Query($portfolio_args);
								if ( $portfolio_loop->have_posts() ) : while ( $portfolio_loop->have_posts() ) : $portfolio_loop->the_post();
							?>
								<div class="column_1_2">
									<a class="portfolio_block" href="<?php the_field('portfolio_link'); ?>" target="_blank">
										<img class="portfolio_image" src="<?php the_field('portfolio_image'); ?>" />
										<div class="portfolio_block_content">
											<div class="portfolio_logo">
												<img src="<?php the_field('portfolio_logo'); ?>" />
											</div>
											<div class="portfolio_description">
												<p><?php the_field('portfolio_description'); ?></p>
												<button href="<?php the_field('portfolio_link'); ?>" target="_blank" class="az_button transparent">Learn About their Impact <img src="<?php echo get_template_directory_uri() . '/img/az_arrow.svg'; ?>" /></button>
											</div>
										</div>
									</a>
								</div>

							<?php endwhile; wp_reset_postdata(); endif; ?>
						</div>
					</div>
				</div>
			</div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>
