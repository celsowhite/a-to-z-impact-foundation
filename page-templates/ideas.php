<?php
/*
Template Name: Ideas
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

			<?php get_template_part('template-parts/header_image'); ?>

			<div class="page_content_container header_image_padding">
				<div class="loading_bar"></div>
				<div class="page_content padding_top_0">
					<?php 
					// AJAX Load more posts. Template in alm_templates > default.php
					echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="10" scroll="false" transition="slide" transition_speed="2000" images_loaded="true" button_label="Load More" button_loading_label="Loading ..."]'); ?>
				</div>
			</div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>
