(function($) {

	$(document).ready(function() {

	"use strict";

		// Only do the AJAX functions if on a desktop
		// Currently AJAX is turned off on the site. If ever want to add it back then uncomment the below.

		if(mq.matches) {

			/*=======================
			Helper Functions
			=======================*/

			// Get the last string of a full URL

			var lastUrlSection = function(url) {
				var initialURLNoLastSlash = url.substr(0, url.length-1);
				var initialSlug = initialURLNoLastSlash.split('/').pop();
				return initialSlug;
			}

			// Change a slug into a title

			var slugToTitle = function(slug) {
				// Remove the dashes
				slug = slug.replace(/-/g,' ');
				// Change the first letter of every word into upper case
				return slug.replace(/(?:^|\s)\w/g, function(match) {
					return match.toUpperCase();
				});
			}

			// Change current menu item

			var changeActiveMenuLink = function(pageSlug) {
				$('.main_navigation li.current-menu-item').removeClass('current-menu-item');
				if(pageSlug !== 'home') {
					$('.main_navigation li.' + pageSlug).addClass('current-menu-item');
				}
			}

			/*=======================
			Set Initial History
			=======================*/

			// Get the URL of the page

			var initialURL = window.location.href;

			var initialSlug;

			if(initialURL === wpSiteURL) {
				initialSlug = 'home';
			}
			else {
				initialSlug = lastUrlSection(initialURL);
			}
			
			var initialTitle = slugToTitle(initialSlug);

			window.history.replaceState(initialSlug, initialTitle, '');

			/*=======================
			PAGE TRANSITION
			Called each time we go to a different URL or change the browser state.
			Performs a smooth transition, clears the current page content and loads in the new content.
			=======================*/

			// Variables

			var ajaxContainer = $('.main_content');
			var loadingScreen = $('.loading_screen');
			var blue = '#1767AA';

			var pageTransition = function(pageSlug) {

				// Set the URL we want grab content from

				var pageURL;

				if(pageSlug !== 'home') {
					pageURL = wpSiteURL + pageSlug;
				}
				else {
					pageURL = wpSiteURL;
				}

				var pageTitle = slugToTitle(pageSlug);

				// Fade out the current page components

				$('.page_header_content').velocity({marginTop: '100px', opacity: 0}, 1000);

				$('.loading_bar').velocity({ width: '0%', backgroundColor: blue }, 1200 );

				// Bring in the loading screen while the hidden page content switches

				loadingScreen.velocity({top: '0%'}, 1000, 'easeOutCubic', function(){

					// Empty the current content on the page

					ajaxContainer.empty();

					// Load in the new pages content

					ajaxContainer.load(pageURL + ' .main_wrapper', function(){

						// Change the browser URL & document title

						window.history.pushState(pageSlug, pageTitle, pageURL);
						document.title = pageTitle;
						$.ajaxSetup({cache: true});
						$.getScript(wpSiteURL + '/wp-content/themes/atozfoundation/js/scripts.min.js');

						// On the ideas page trigger the load more for AJLM plugin.

						if(pageSlug === 'ideas') {
							$('.ajax-load-more-wrap').ajaxloadmore();
						}
						
						// Special class handling for homepage structure

						if($('body').hasClass('page-template-homepage')) {
							$('body').removeClass('page-template-homepage');
						}
						
						if(pageSlug === 'home') {
							$('body').addClass('page-template-homepage');
						}

						// Hide the loading screen to reveal the new content

						loadingScreen.velocity({width: '0%'}, 1000, function(){
							// Page entrance transitions happen on page load via custom/general.js
							// Set the loading screen back to its original positioning
							loadingScreen.removeAttr('style');
						});
					});

				});
			};

			/*=======================
			On click of a menu link run the ajax call and transition the page.

			$('.main_header .main_navigation a, .main_header a.logo, .ajax_link').click(function(e){

				e.preventDefault();

				// Check if the current page is a single post.

				var currentPage = $('.main_wrapper').attr('data-page');

				// Capture the URL and info of the page we are transitioning to

				var pageURL = $(this).attr('href');
				var pageSlug;
				if(pageURL === wpSiteURL) {
					pageSlug = 'home';
				} 
				else {
					pageSlug = lastUrlSection(pageURL);
				}

				var scrollPosition = $(window).scrollTop();

				// Change header theme depending on which page the user is transitioning to

				var mainHeader = $('.main_header');

				if(currentPage === 'single') {
					mainHeader.removeClass('dark');
				}
				
				// If the user is not at the top of the page then first scroll to the top then trigger ajax functions

				if(scrollPosition > 0) {
					$('html').velocity("scroll", { duration: 1500, easing: "easeInOut", complete: function(){
						window.setTimeout(function(){
							pageTransition(pageSlug);
							changeActiveMenuLink(pageSlug);
						}, 800);
					}});
				}
				
				else {
					pageTransition(pageSlug);
					changeActiveMenuLink(pageSlug);
				}
			});

			=======================*/

			/*=======================
			On popstate run the page transition function.

			window.addEventListener('popstate', function(e) {
				pageTransition(e.state);
				changeActiveMenuLink(e.state);
			});
			
			=======================*/
		};

	});

})(jQuery);