(function($) {

	$(document).ready(function() {

		var sliderContent = $('.homepage_slider_content');
		var sliderTitle = $('.homepage_slider_content h1');
		var sliderDescription = $('.homepage_slider_content p');

		/*================================= 
		HOMEPAGE SPLASH SCREEN
		=================================*/

		const splashScreen = $('.splash_screen');
		const loadingLogo = document.querySelectorAll('.logo_transition');
		const firstSVGPath = loadingLogo[0];

		const hideSplashScreen = function() {
			splashScreen.delay(1000).fadeOut({
				duration: 1000,
				complete: function(){
					sliderContent.velocity({opacity: 1}, 1000);
					sliderTitle.velocity({translateY: '0%'}, 1000);
					sliderDescription.velocity({translateY: '0%'}, 1000);
					localStorage.setItem('viewedAnimation', 'yes');
				}
			});
		};

		// Run the page transition elements on timeout function only if the animation has already been viewed.

		if(localStorage.getItem('viewedAnimation') === 'yes') {
			window.setTimeout(function(){
				sliderContent.velocity({opacity: 1}, 1000);
				sliderTitle.velocity({translateY: '0%'}, 1000);
				sliderDescription.velocity({translateY: '0%'}, 1000);
			}, 300);
		}
		else {

			// Hide the splash screen. Within this function is a callback to load in the pages contents.
			
			window.setTimeout(hideSplashScreen, 2000);
		}

		/*================================= 
		HOMEPAGE SLIDER
		=================================*/

		$('.homepage_slider').flexslider({
			directionNav: false,
			controlsContainer: '.homepage_slider_content'
		});
	
	});

})(jQuery);