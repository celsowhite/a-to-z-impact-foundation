(function($) {

  $(document).ready(function() {

  "use strict";

    /*================================= 
    Ajax Load More
    =================================*/

    var loadButton = $('.alm-load-more-btn');

    // Complete Callback

    $.fn.almComplete = function(alm){

        // Find all youtube or vimeo iFrame elements

        var mediaFrames = $('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]');

        // Iterate over each iFrame to check if it should add the video_embed wrapper

        mediaFrames.each(function() {
          if ( $(this).parent(".fluid-width-video-wrapper").length == 1 ) {}
          else {
            $(this).wrap("<div class='video_embed'/>");
          }
        });

        /*=== Target div for fitVids ===*/

        $(".video_embed").fitVids();

    };

    // Done Callback

    $.fn.almDone = function(){

      loadButton.text('No additional posts.');

    };

  });

})(jQuery);