(function($) {

	$(document).ready(function() {

	"use strict"; 

		/*================================= 
		EXPAND PANELS
		=================================*/

		var firstPanel = $('.individual_panel:nth-child(1)');
		var secondPanel = $('.individual_panel:nth-child(2)');
		var thirdPanel = $('.individual_panel:nth-child(3)');
		var fourthPanel = $('.individual_panel:nth-child(4)');

		var hoverSpeed = 600;
		var hoverInEasing = 'easeInSine';
		var hoverOutEasing = 'easeOutSine';

		var panelTransitionStop = function() {
			firstPanel.velocity('stop');
			secondPanel.velocity('stop');
			thirdPanel.velocity('stop');
			fourthPanel.velocity('stop');
		};

		var panelHoverOut = function() {
			panelTransitionStop();
			$('.individual_panel.inactive_panel').removeClass('inactive_panel');
		};

		// First Panel

		var firstHoverIn = function() {
			panelTransitionStop();
			secondPanel.add(thirdPanel).add(fourthPanel).addClass('inactive_panel');
		};

		// Second Panel

		var secondHoverIn = function() {
			panelTransitionStop();
			firstPanel.add(thirdPanel).add(fourthPanel).addClass('inactive_panel');
		};

		// Third Panel

		var thirdHoverIn = function() {
			panelTransitionStop();
			firstPanel.add(secondPanel).add(fourthPanel).addClass('inactive_panel');
		};

		// Fourth Panel

		var fourthHoverIn = function() {
			panelTransitionStop();
			firstPanel.add(secondPanel).add(thirdPanel).addClass('inactive_panel');
		};

		if(mq.matches) {
			firstPanel.hover(firstHoverIn, panelHoverOut);
			secondPanel.hover(secondHoverIn, panelHoverOut);
			thirdPanel.hover(thirdHoverIn, panelHoverOut);
			fourthPanel.hover(fourthHoverIn, panelHoverOut);
		}

	});

})(jQuery);