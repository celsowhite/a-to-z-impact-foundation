(function($) {

  $(document).ready(function() {

  "use strict";

    /*================================= 
    Team Lightbox
    =================================*/

    $('.team_member_block').click(function(){

      // Save team member info

      var teamMemberImage = $(this).find('img').attr('src');
      var teamMemberName = $(this).find('h2').text();
      var teamMemberTitle = $(this).find('p.team_title').text();
      var teamMemberDescription = $(this).find('.hidden_team_description').html();

      // Save lightbox element variables

      var lightbox = $('.team_member_lightbox_container');
      var lightboxImage = lightbox.find('img');
      var lightboxName = lightbox.find('h2');
      var lightboxTitle = lightbox.find('p');
      var lightboxDescription = lightbox.find('.lightbox_description');

      // Input elements into the lightbox

      lightbox.fadeIn('slow');
      lightboxImage.attr('src', teamMemberImage);
      lightboxName.text(teamMemberName);
      lightboxTitle.html('<b>' + teamMemberTitle + '</b>');
      lightboxDescription.html(teamMemberDescription);

      // Set the body class so there is no scroll on the page

      $('body').addClass('lightbox_open');

    });

    // Close Lightbox

    $('.team_member_lightbox .close_icon').click(function(){
      $('.team_member_lightbox_container').fadeOut('slow');
      $('body').removeClass('lightbox_open');
    });

  });

})(jQuery);