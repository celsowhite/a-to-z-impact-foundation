(function($) {

	$(document).ready(function() {

	"use strict";

		// Variables

		var mainContent = $('.main_content');
		var pageHeader = $('.page_header');
		var pageHeaderContent = $('.page_header_content');
		var pageHeaderContentTitle = $('.page_header_content h1');
		var pageBorder = $('.loading_bar') ;
		var pageContent = $('.page_content');
		var blue = '#1767AA';
		var lightBlue = '#8BC2E8';

		/*================================= 
		PAGE LOADED
		=================================*/

		window.setTimeout(function(){
			mainContent.velocity({opacity: 1}, 1000, function(){
				pageBorder.velocity({ width: '100%', backgroundColor: lightBlue }, 1500);
				pageContent.velocity({opacity: 1}, 1000);
				pageHeaderContent.velocity({marginTop: '0px', opacity: 1}, 1000);
			});
		}, 300);

		/*================================= 
		PAGE HEADER PARALLAX
		=================================*/

		$(window).scroll(function(){
			var scrollPosition = $(window).scrollTop();
			var offsetTop = -($(window).scrollTop()/3);
			pageHeaderContentTitle.css({
				'webkit-transform':'translateY(' + offsetTop + 'px)'
			});
			if (scrollPosition >= 3) {
				pageHeaderContent.velocity('stop');
				pageHeaderContent.velocity({opacity: 0, scale: .9}, 100);
			}
			else {
				pageHeaderContent.velocity('stop');
				pageHeaderContent.velocity({opacity: 1, scale: 1}, 100);
			}
		});

		/*================================= 
		SOCIAL SHARE
		=================================*/

		/*== Facebook Share ==*/

		$('a.fb_share').click(function(e) {

			e.preventDefault();

			var loc = $(this).attr('href');

			window.open('http://www.facebook.com/sharer.php?u=' + loc,'facebookwindow','height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

		});

		$('a.twitter_share').click(function(e){

		    e.preventDefault();

		    var loc = $(this).attr('href');

		    var via = $(this).attr('via');

		    var title  = encodeURIComponent($(this).attr('title'));

		    window.open('http://twitter.com/share?url=' + loc, 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

		});

		/*================================= 
		FITVIDS
		=================================*/

		/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

		$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

		/*=== Target div for fitVids ===*/

		$(".video_embed").fitVids();

	});

})(jQuery);


