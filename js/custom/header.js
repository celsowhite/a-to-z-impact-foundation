(function($) {

	$(document).ready(function() {

	"use strict"; 

		/*================================= 
		TRANSFORM HEADER ON SCROLL
		=================================*/

		$(window).scroll(function() {
			var scrollPosition = $(window).scrollTop();
			var mainHeader = $('.main_header');
			if (scrollPosition >= 30) {
				mainHeader.addClass('scrolled');
			}
			else {
				mainHeader.removeClass('scrolled');
			}
		});

		/*================================= 
		MOBILE NAVIGATION REVEAL
		=================================*/

		$('.menu_icon').click(function(){
			$('.mobile_menu').toggleClass('open');
		});

	});

})(jQuery);