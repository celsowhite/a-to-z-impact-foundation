<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

			<?php get_template_part('template-parts/header_image'); ?>

			<div class="page_content_container <?php if(get_field('page_header_image')): ?>header_image_padding<?php else: ?>single_padding<?php endif; ?>">
				<?php if(get_field('page_header_image')): ?>
					<div class="loading_bar"></div>
				<?php endif; ?>
				<div class="page_content wysiwyg">
					<div class="single_column_container">
						<?php if(!get_field('page_header_image')): ?>
							<h1><?php the_title(); ?></h1>
						<?php endif; ?>
						<?php the_content(); ?>
					</div>
				</div>
			</div>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>
