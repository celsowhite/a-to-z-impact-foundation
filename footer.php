<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	</div>

	<div class="team_member_lightbox_container">
		<div class="team_member_lightbox_flex_container">
			<div class="team_member_lightbox">
				<img src="" />
				<header>
					<h2></h2>
					<p></p>
				</header>
				<div class="lightbox_description"></div>
				<span class="close_icon"></span>
			</div>
		</div>
	</div>

	<footer class="main_footer">
		<div class="footer_bottom">
			<div class="container">
				<p>©<?php echo date("Y"); ?> A to Z Impact</p>
				<?php wp_nav_menu( array( 'menu' => 'footer-menu', 'menu_class' => 'footer_menu', 'container' => '' ) ); ?>
			</div>
		</div>
	</footer>

</div>

<?php wp_footer(); ?>

<script>
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-91047001-1']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>

</body>
</html>
