<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" data-page="single" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php
			// Get categories for this specific post
			$category_names = [];
			foreach(get_the_category($post) as $category) {
				$category_names[] = $category->name;
			}
			?>

			<div class="page_content_container single_padding">
				<div class="page_content_border"></div>
				<div class="page_content">
					<div class="single_column_container">

						<header class="single_post_header">
							<h2><?php the_title(); ?></h2>
							<div class="single_post_meta">
								<span><?php the_date('m/d/Y'); ?> | <?php echo implode(', ', $category_names); ?></span>
								<span>SHARE:
									<ul class="social_share">
										<li><a class="fb_share" href="<?php the_permalink(); ?>"><i class="fa fa-facebook"></i></a></li>
										<li><a class="twitter_share" href="<?php the_permalink(); ?>"><i class="fa fa-twitter"></i></a></li>
										<li><a href="mailto:info@atozfoundation.org"><i class="fa fa-envelope"></i></a></li>
									</ul>
								</span>
							</div>
						</header>

						<section class="single_post_featured_media">
							<?php if(get_field('idea_post_media_type') === 'video'): ?>
								<?php the_field('idea_post_video_embed'); ?>
							<?php else: ?>
								<img src="<?php the_field('idea_post_image'); ?>" />
							<?php endif; ?>
						</section>

						<section class="single_post_content">
							<?php the_content(); ?>
						</section>

						<section class="single_post_navigation">
							<div class="previous_post">
								<?php if(get_previous_post_link()): ?>
									<h2>Previous Post</h2>
									<?php previous_post_link('%link'); ?>
								<?php endif; ?>
							</div>
							<div class="next_post">
								<?php if(get_next_post_link()): ?>
									<h2>Next Post</h2>
									<?php next_post_link('%link'); ?>
								<?php endif; ?>
							</div>
						</section>

					</div>
				</div>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
