<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="icon" type="img/png" href="<?php echo get_template_directory_uri() . '/favicon.png'; ?>" />

<?php wp_head(); ?>

<!-- Save base wordpress URL for usage in ajax -->

<script type="text/javascript">
	var wpSiteURL = '<?php echo esc_url( home_url( '/' ) ); ?>';
	var mq = window.matchMedia( '(min-width: 1024px)' );
</script>

</head>

<body <?php body_class(); ?>>

<div id="page">

	<header class="mobile_header">
		<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<img src="<?php echo get_template_directory_uri() . '/img/logo_people.svg' ?>" />
		</a>
		<span class="menu_icon"></span>
	</header>

	<nav class="mobile_navigation">
		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'mobile_menu', 'menu_id' => '', 'container' => '' ) ); ?>
	</nav>

	<header class="main_header <?php if(is_single() || (!is_front_page() && !get_field('page_header_image'))): ?>dark<?php else: ?>light<?php endif; ?>">
		<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<img class="light_logo" src="<?php echo get_template_directory_uri() . '/img/logo_light_transparent.svg' ?>" />
			<img class="dark_logo" src="<?php echo get_template_directory_uri() . '/img/logo_dark_transparent.svg' ?>" />
			<img class="people_logo" src="<?php echo get_template_directory_uri() . '/img/logo_people.svg' ?>" />
		</a>
		<nav class="main_navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'main-menu', 'container' => '' ) ); ?>
		</nav>
	</header>

	<section class="splash_screen">
		<?php echo get_template_part('template-parts/logo_svg'); ?>
		<script type="text/javascript">
			// Script to hide/show the splash screen depending on if the viewer has already seen it.
			var viewedAnimation = localStorage.viewedAnimation;
			if(viewedAnimation !== 'yes') {
				document.querySelector('body').classList.add('showAnimation');
			}
		</script>
	</section>

	<div id="content" class="main_content">