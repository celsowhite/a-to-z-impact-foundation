<?php
// Header Image component used for parallax header transitions.
?>

<?php if(get_field('page_header_image')): ?>
	<header class="page_header" style="background-image: url('<?php the_field('page_header_image') ?>'); background-position: <?php the_field('page_header_image_position'); ?>">
		<?php if(get_field('page_header_text')): ?>
			<div class="container">
				<div class="page_header_content">
					<h1><?php the_field('page_header_text') ?></h1>
				</div>
			</div>
		<?php endif; ?>
	</header>
<?php endif; ?>